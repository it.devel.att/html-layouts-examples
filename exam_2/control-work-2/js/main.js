jQuery(function($){


	$('.promoSlider').slick({
		dots: true,
		speed: 300,
		autoplay: true,
		autoplaySpeed: 3000,
		infinite: true,
		responsive: [
		{
			breakpoint: 800,
			settings: {
				autoplay: true,
				autoplaySpeed: 2000,
				infinite: true,
				arrows: false,
				dots: true
			}
		},
		{
			breakpoint: 470,
			settings: {
				arrows: false,
				dots: false
			}
		}
		]
	});


});